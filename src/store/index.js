import { createStore } from 'vuex'

export default createStore({
  state() {
    return {
      authToken: "",
      counter: 0
    }
  },
  mutations: {
    setToken(state, payload) {
      state.authToken = payload.token;
    },
    increment(state) {
      state.counter = state.counter + 1;
    },
    increase(state, payload) {
      state.counter = state.counter + payload.value;
    },
    removeToken(state) {
      state.authToken = "";
      localStorage.removeItem('sendify-token');
    },
  },
  getters: {
    getToken(state) {
      return state.authToken || localStorage.getItem('sendify-token');
    },
    finalCounter(state) {
      return state.counter;
    }
  },
  actions: {
    increment(context) {
      setTimeout(function() {
        context.commit('increment');
      }, 1500);
    },
    increase(context, payload) {
      setTimeout(function() {
        context.commit('increase', payload);
      }, 1500);
    }
  },
  modules: {
  }
})
