import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Viewer from '../views/Viewer.vue'
import Editor from '../views/Editor.vue'
import Library from '../views/Library.vue'
import Recipients from '../views/Recipients.vue'
import Mailer from '../views/Mailer.vue'
import Authorization from '../views/Authorization.vue'
import Activity from '../views/Activity.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/recipients',
    name: 'Recipients',
    component: Recipients
  },
  {
    path: '/mailer',
    name: 'Mailer',
    component: Mailer
  },
  {
    path: '/activity',
    name: 'Activity',
    component: Activity
  },
  {
    path: '/viewer/:id/view',
    name: 'Viewer',
    component: Viewer
  },
  {
    path: '/editor/:id/edit',
    name: 'Editor',
    component: Editor
  },
  {
    path: '/library',
    name: 'Library',
    component: Library
  },
  {
    path: '/authorization',
    name: 'Authorization',
    component: Authorization
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
